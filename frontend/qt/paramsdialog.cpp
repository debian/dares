/* paramsdialog.cpp */
/* part of DAta REScue dares */

/* DAta REScue (C) 2002 Oliver Diedrich, odi@ct.heise.de */
/* (C) 2005 c't magazine for computer technology, Heise Zeitschriften Verlag */
/* This file may be redistributed under the terms of the */
/* GNU Public License (GPL), see www.gnu.org */

#include "paramsdialog.h"

#include <qlayout.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qcheckbox.h>
#include <qhbox.h>
#include <qpushbutton.h>
#include <qfiledialog.h>

ParamsDialog::ParamsDialog( const QString& imageName,
                            const QString& savePath,
                            const QString& h2iFileName,
                            const QString& logName,
                            bool useMIME,
                            QWidget* parent,
                            const char* name,
                            bool modal,
                            WFlags f )
  : QDialog( parent, name, modal, f ),
    mImageName(   imageName ),
    mSavePath(    savePath ),
    mH2iFileName( h2iFileName ),
    mLogName(     logName ),
    mUseMIME(     useMIME )
{
  setCaption( "DAta REScue (C) 2005 c't" );
  resize( 540, 40 );

  QGridLayout* grid = new QGridLayout( this, 7, 3, 16 );
  grid->setColStretch( 0, 0 );
  grid->setColStretch( 1, 1 );
  grid->setColStretch( 2, 0 );

  QLabel* label = new QLabel( this );
  label->setText( "Enter parameters or press <i>Cancel</i> to quit." );
  label->setAlignment( AlignCenter );
  grid->addMultiCellWidget( label, 0, 0, 0, 2 );

  mEditImageName = new QLineEdit( mImageName, this );
  label = new QLabel( "&Image name:", this );
  label->setBuddy( mEditImageName );
  grid->addWidget( label,          1, 0 );
  grid->addWidget( mEditImageName, 1, 1 );
  QPushButton* button = new QPushButton( "Select ...", this );
  grid->addWidget( button,         1, 2 );
  connect( button, SIGNAL( clicked() ), this, SLOT( selectImageName() ) );

  mEditSavePath = new QLineEdit( mSavePath, this );
  label = new QLabel( "Path for &saving:", this );
  label->setBuddy( mEditSavePath );
  grid->addWidget( label,           2, 0 );
  grid->addWidget( mEditSavePath,   2, 1 );
  button = new QPushButton( "Select ...", this );
  grid->addWidget( button,          2, 2 );
  connect( button, SIGNAL( clicked() ), this, SLOT( selectSavePath() ) );

  mEditH2iFileName = new QLineEdit( mH2iFileName, this );
  label = new QLabel( "&H2i file name:", this );
  label->setBuddy( mEditH2iFileName );
  grid->addWidget( label,           3, 0 );
  grid->addWidget( mEditH2iFileName,3, 1 );
  button = new QPushButton( "Select ...", this );
  grid->addWidget( button,          3, 2 );
  connect( button, SIGNAL( clicked() ), this, SLOT( selectH2iFileName() ) );

  mEditLogName = new QLineEdit( mLogName, this );
  label = new QLabel( "&Logfile name:", this );
  label->setBuddy( mEditLogName );
  grid->addWidget( label,          4, 0 );
  grid->addWidget( mEditLogName,   4, 1 );
  button = new QPushButton( "Select ...", this );
  grid->addWidget( button,         4, 2 );
  connect( button, SIGNAL( clicked() ), this, SLOT( selectLogFile() ) );

  mCheckUseMIME = new QCheckBox( "use &MIME types", this );
  mCheckUseMIME->setChecked( mUseMIME );
  grid->addWidget( mCheckUseMIME,  5, 1 );

  QHBox* box = new QHBox( this );
  box->setSpacing( 128 );
  grid->addMultiCellWidget( box, 6, 6, 0, 2 );

  button = new QPushButton( "&OK", box );
  button->setDefault( TRUE );
  connect( button, SIGNAL( clicked() ), this, SLOT( accept() ) );

  button = new QPushButton( "Cancel", box );
  button->setDefault( false );
  button->setAccel( Key_Escape );
  connect( button, SIGNAL( clicked() ), this, SLOT( reject() ) );
}


void ParamsDialog::selectImageName()
{
  mImageName = QFileDialog::getOpenFileName( mImageName, QString::null, this, "open file dialog", "Choose a file (or device, resp.) to open" );
  mEditImageName->setText( mImageName );
}

void ParamsDialog::selectSavePath()
{
  mSavePath = QFileDialog::getExistingDirectory( mSavePath, this, "get dir dialog", "Choose a directory for storing saved files" );
  if( mSavePath.endsWith("/") || mSavePath.endsWith("\\") )
    mSavePath.truncate( mSavePath.length()-1 );
  mEditSavePath->setText( mSavePath );
}

void ParamsDialog::selectH2iFileName()
{
  mH2iFileName = QFileDialog::getOpenFileName( mH2iFileName, QString::null, this, "open file dialog", "Choose h2i file for image (as written by H2cdimage)" );
  mEditH2iFileName->setText( mH2iFileName );
}

void ParamsDialog::selectLogFile()
{
  mLogName = QFileDialog::getSaveFileName( mLogName, QString::null, this, "open file dialog", "Choose a log file to store data in" );
  mEditLogName->setText( mLogName );
}

void ParamsDialog::accept()
{
  mImageName   = mEditImageName->text();
  mSavePath    = mEditSavePath->text();
  mH2iFileName = mEditH2iFileName->text();
  mLogName     = mEditLogName->text();
  mUseMIME     = mCheckUseMIME->isChecked();

  QDialog::accept();
}
