/* dares.h */
/* part of DAta REScue dares */

/* DAta REScue (C) 2002 Oliver Diedrich, odi@ct.heise.de */
/* (C) 2005 c't magazine for computer technology, Heise Zeitschriften Verlag */
/* This file may be redistributed under the terms of the */
/* GNU Public License (GPL), see www.gnu.org */

#ifndef __DARES_H__
#define __DARES_H__

#define NAME_FORMAT "%s/%07lu-%s.%s"    // save_path, block #, type, bin|asc
#define BIN 1                           // binary file
#define ASC 2                           // text file
#define END_NULL_BIN 5                  // # zeros at end of binary files
#define END_NULL_ASC 1                  // # zeros at end of text files
#define LOG_FILE "dares.log"            // default name of log file

#define MAXMAGIS 100000                 // different file types in all_type[]
#define MAX_TYPE 80                     // max length of file type string
#define MAGIC_FILE "magic"              // default name of magic file

// define DOS to compile for DOS/Windows environment
#undef DOS
#ifndef DOS
  #define O_BINARY 0
#endif


// daresOpen() return values:
// return values greater than DARES_OPEN_OK contain the number of blocks found
#define DARES_OPEN_OK                 0
#define DARES_OPEN_INVALID_PARAM     -1
#define DARES_OPEN_CANNOT_OPEN_IMAGE -2
#define DARES_OPEN_CANNOT_MALLOC     -3
#define DARES_OPEN_CANNOT_OPEN_MAGIC -4

/* save_file() return values */
#define SAVE_FILE_OK                   1
#define SAVE_FILE_CANNOT_OPEN_IMAGE   -1
#define SAVE_FILE_CANNOT_OPEN_OUTFILE -2
#define SAVE_FILE_SEEK_ERROR          -3
#define SAVE_FILE_READ_ERROR          -4
#define SAVE_FILE_WRITE_ERROR         -5

#undef NDEBUG
#include <assert.h>

struct extent                 // a series of block numbers belonging to a file
{
  unsigned long start;        // block number
  unsigned long end;          // block number
  struct extent *next;        // next extent; NULL: last extent
};


struct one_file               // a recognized file while scanning disk
{
  unsigned long count;        // current number
  struct extent *ext;         // pointer to first extent
  unsigned long size;         // # bytes already written
  int filetype;               // ASC or BIN
  char *trunc;                // "truncated: ..." string, might be missing
  struct one_file *next;      // pointer to next file of same type
};


struct one_type               // one of the found file types
{
  char *name;                 // file type
  unsigned int hash;          // hash value, computed from name; after scan:
                              // # of files of this type
  unsigned int trunc;         // # of truncated files
  struct one_file *first;     // start of the list of files of this type
  struct one_file *last;      // last element in this list
};


int daresOpen(int argc, char *argv[]);

int daresClose( void );

int save_file(struct one_file *f, char *type, char *name);

#endif

