all	: qt ncurses

qt	:
	cd frontend/qt && qmake && make

ncurses	:
	cd frontend/ncurses && make

clean	:
	cd frontend/ncurses && make clean
	cd frontend/qt && make distclean

